const client = require('./elastic.service');
const shortid = require('./shortid.service');

module.exports = {
	addBulk: async (item = {}) => {
		try {
			await client
				.bulk({
					body: item,
				})
				.catch((e) => {
					console.log(e);
					throw 'elastic update bulk  failed';
				});
		} catch (e) {
			console.log(e);
			throw 'elastic update bulk  failed';
		}
	},
	addItem: async (index = '', item = {}) => {
		try {
			await client
				.create({
					index: index,
					type: '_doc',
					id: shortid.generate(),
					body: item,
				})
				.catch((e) => {
					throw 'elastic cron  failed';
				});
		} catch (e) {
			console.log('elastic add Item  failed');
			console.log(e);
			throw 'elastic add Item  failed';
		}
	},
	deleteItem: async (index = '', item = {}) => {
		await client
			.deleteByQuery({
				index: index,
				body: {
					query: {
						match: item,
					},
				},
			})
			.catch((e) => {
				console.log('elastic delete Item  failed');
				console.log(e);
				throw 'elastic delete Item  failed';
			});
	},
	deleteIndex: async (index = '') => {
		await client.indices.delete({ index: index }).catch((e) => {
			console.log('elastic delete Index  failed');
			console.log(e);
		});
	},
	createIndex: async (index = '') => {
		await client.indices.create({ index: index }).catch((e) => {
			console.log('Elastic create Index  failed');
			console.log(e);
		});
	},
	// search: async (
	// 	index = '',
	// 	must = [],
	// 	must_not = [],
	// 	should = [], 
	// 	size = 10,
	// 	from = 0,
	// 	sort = null
	// ) => {
	// 	let result;
	// 	let objectToAPi = {
	// 		index: index,
	// 		type: '_doc',
	// 		size: size,
	// 		from: from,
	// 		ignoreUnavailable: true,
	// 		body: {
	// 			query: {
	// 				bool: {
	// 					should: should,
	// 					must: must,
	// 					must_not: must_not,
	// 				},
	// 			},
	// 		},
	// 	};
	// 	if (!!sort && should.length === 0) {
	// 		//objectToAPi.body.sort = sort;
	// 	}
	// 	result = await client.search(objectToAPi);
	// 	return result;
	// },
};