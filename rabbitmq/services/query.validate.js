module.exports = {
    validate: (req, res, next) => {
        console.log(req.query);
        let options = {
			should: [],
			must: [],
			must_not: [],
			limit: req.query.limit || 10,
			offset: req.query.offset || 0,
			order: [{ createdAt: { order: 'desc' } }],
		};
        if(req.query.should)
            options.should = options.should.concat(req.query.should);
        if(req.query.must)
            options.must = options.must.concat({ match: {username: req.query.must} })
        req.options = options;
        next();
    }
}