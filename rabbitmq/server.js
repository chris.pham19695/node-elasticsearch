const server = require('./app');
const port   = 1906;

server.listen(port, () => {
    console.log('Server Listen On:', port);
});