const amqp           = require('amqplib/callback_api');
const ElasticService = require('../../api/services/elastic.util');

const configItemsToElastic = async (index = '', type = '', result = {}) => {
    let id = ''
    let item = {};
    if(result.id && index === 'users') {
        id = { id: result.id },
        item = {
            id: result.id,
            email: result.email,
            name: result.name
        }
    }
    switch (type) {
        case 'create':
            await ElasticService.addItem(index, item);
            break;
        case 'remove':
            await ElasticService.deleteItem(index, id);
            break;
        case 'update':
            await ElasticService.deleteItem(index, id);
            await ElasticService.addItem(index, item)
            break;
        default:
            break;
    }
}

module.exports = () => {
    amqp.connect('amqp://opla:kr2h4YzRH5LakSad@172.16.0.122:9013', (err, connection) => {
        if (err) {
            throw err;
        }
        console.log('RabbitMQ Connected')
        connection.createChannel((error, channel) => {
            if (error) {
                throw error;
            }
            const exchange = 'opla.users';
            channel.assertExchange(exchange, 'direct', {
                durable: false
            })
            console.log('Waiting for messages in Queue');
            channel.assertQueue('', {
                exclusive: true
              }, function(error2, q) {
                if (error2) {
                  throw error2;
                }
                channel.bindQueue(q.queue, exchange, 'create');
                channel.bindQueue(q.queue, exchange, 'remove');
                channel.bindQueue(q.queue, exchange, 'update');
                channel.consume(q.queue, (results) => {
                    const result = JSON.parse(results.content)
                    switch (results.fields.exchange) {
                        case 'opla.users':
                            configItemsToElastic('users', results.fields.routingKey, result)
                            break;
                        case 'opla.posts':
                            configItemsToElastic('posts', results.fields.routingKey, result)
                            break;
                        default:
                            break;
                    }
                }, {
                  noAck: true
                });
            });
        });
    })
}
