const shortid = require('./../../services/shortid.service');
const ElasticService = require('./../../services/elastic.util');

module.exports = {
	update: async (req, res, next) => {
		console.log('Middleware : elastic update create User');
		try {
			await ElasticService.deleteIndex('users');
			await ElasticService.createIndex('users');
			let dataUsers = req.data.Objectives || [];
			let ElasticObject = [];
			for (let i = 0; i < dataUsers.length; i++) {
				ElasticObject.push({
					index: {
						_index: 'users',
						_type: '_doc',
						_id: shortid.generate(),
					},
				});
				ElasticObject.push({
					username: dataUsers[i].user.username,
					email: dataUsers[i].user.email,
				});
			}
			await ElasticService.addBulk(ElasticObject);
			next();
		} catch (e) {
			console.log(e);
			throw 'elastic create users failed';
		}
	},
	updateSingle: async (req, res, next) => {
		console.log('Middleware : elastic create User');
		try {
			let ElasticObject = {
				username: req.body.username,
				email: req.body.email,
			};
			await ElasticService.addItem('users', ElasticObject);
			next();
		} catch (e) {
			console.log(e);
			throw 'elastic create user failed';
		}
	},
	deleteSingle: async (req, res, next) => {
		console.log('Middleware : elastic create User');
		try {
			let ElasticObject = {
				email: req.body.email,
			};
			await ElasticService.deleteItem('users', ElasticObject);
			next();
		} catch (e) {
			console.log(e);
			throw 'elastic create user failed';
		}
	},
};