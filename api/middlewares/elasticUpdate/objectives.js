const shortid = require('./../../services/shortid.service');
const ElasticService = require('./../../services/elastic.util');

module.exports = {
	update: async (req, res, next) => {
		console.log('Middleware : elastic update create Objective');
		try {
			await ElasticService.deleteIndex('objectives');
			await ElasticService.createIndex('objectives');
			let dataObjectives = req.data.Objectives || [];
			let ElasticObject = [];
			for (let i = 0; i < dataObjectives.length; i++) {
				ElasticObject.push({
					index: {
						_index: 'objectives',
						_type: '_doc',
						_id: shortid.generate(),
					},
				});
				ElasticObject.push({
					id: dataObjectives[i].id,
					name: dataObjectives[i].name,
					deadline: dataObjectives[i].deadline,
					createdAt: dataObjectives[i].createdAt,
					complete: dataObjectives[i].complete,
				});
			}
			await ElasticService.addBulk(ElasticObject);
			next();
		} catch (e) {
			console.log(e);
			throw 'elastic update objective failed';
			next();
		}
	},

	updateSingle: async (req, res, next) => {
		console.log('Middleware : elastic update create Objective');
		try {
			let ElasticObject = {
				id: req.body.id,
				name: req.body.name,
				deadline: req.body.deadline,
				createdAt: req.body.createdAt,
				complete: req.body.complete,
			};
			await ElasticService.addItem('objectives', ElasticObject);
			next();
		} catch (e) {
			console.log(e);
			throw 'elastic create objective failed';
			next();
		}
	},

	deleteSingle: async (req, res, next) => {
		console.log('Middleware : elastic create User');
		try {
			let ElasticObject = {
				id: req.body.id,
			};
			await ElasticService.deleteItem('objectives', ElasticObject);
			next();
		} catch (e) {
			console.log(e);
			throw 'elastic create user failed';
		}
	},
};