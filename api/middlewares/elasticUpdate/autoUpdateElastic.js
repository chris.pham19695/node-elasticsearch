const Axios = require('axios');
const config = require('./../../config/config');
const query = require('./../../services/query.service');
const ElasticService = require('./../../services/elastic.util')
const shortid = require('./../../services/shortid.service');

module.exports = {
	updateObjectives: async () => {
		await Axios({
			url: config.ObjectiveUrlApi,
			method: 'post',
			data: {
				query: query.getObjectives,
			},
		})
			.then(async (res) => {
                try {
                    await ElasticService.deleteIndex('objectives');
                    await ElasticService.createIndex('objectives');
                    let dataObjectives = res.data.data.Objectives || [];
                    let ElasticObject = [];
                    for (let i = 0; i < dataObjectives.length; i++) {
                        ElasticObject.push({
                            index: {
                                _index: 'objectives',
                                _type: '_doc',
                                _id: shortid.generate(),
                            },
                        });
                        ElasticObject.push({
                            id: dataObjectives[i].id,
                            name: dataObjectives[i].name,
                            deadline: dataObjectives[i].deadline,
                            createdAt: dataObjectives[i].createdAt,
                            complete: dataObjectives[i].complete,
                        });
                    }
                    await ElasticService.addBulk(ElasticObject);
                } catch (e) {
                    console.log(e);
                    throw 'elastic update objective failed';
                }
			})
			.catch((err) => {
				console.log(err);
			});
	},
	updateUser: async () => {
		await Axios({
			url: config.ObjectiveUrlApi,
			method: 'post',
			data: {
				query: query.getUsers,
			},
		})
			.then(async (res) => {
                try {
                    await ElasticService.deleteIndex('users');
                    await ElasticService.createIndex('users');
                    let dataUsers = res.data.data.Objectives || [];
                    let ElasticObject = [];
                    for (let i = 0; i < dataUsers.length; i++) {
                        ElasticObject.push({
                            index: {
                                _index: 'users',
                                _type: '_doc',
                                _id: shortid.generate(),
                            },
                        });
                        ElasticObject.push({
                            username: dataUsers[i].user.username,
                            email: dataUsers[i].user.email,
                        });
                    }
                    await ElasticService.addBulk(ElasticObject);
                } catch (e) {
                    console.log(e);
                    throw 'elastic create users failed';
                }
			})
			.catch((err) => console.log(err));
	},
};
