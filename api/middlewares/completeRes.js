
module.exports = (req, res, next) => {
    console.log('Middleware : Complete res');
    try {
        if(req.return)
            return res.status(req.status).send(req.return);
        else
            return res.status(400).send({Global: 'Error'});
    }
    catch(e) {
        console.log(e);
        next();
    }
};
