const express = require('express');
const router = express.Router();
const searchCtrl = require('./../controllers/searchCtrl');
const completeRes = require('./../middlewares/completeRes');
const queryValidate = require('./../services/query.validate');


router
	.route('/objectives')
	.post(
        queryValidate.validate, 
        searchCtrl.objectives, 
        completeRes
    );

router
	.route('/users')
	.post(
        queryValidate.validate, 
        searchCtrl.users, 
        completeRes
    );

module.exports = router;
