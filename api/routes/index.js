const express       = require('express');
const router        = express.Router();


router.use('/search', require('./searchRoute'));


module.exports = router;