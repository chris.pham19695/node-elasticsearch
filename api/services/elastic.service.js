
// IMPORTS
const Client = require('elasticsearch').Client;

module.exports = new Client({
    host: 'localhost:9200',
    log: 'trace',
});
