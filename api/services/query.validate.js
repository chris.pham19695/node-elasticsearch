module.exports = {
    validate: (req, res, next) => {
        //console.log(req.body.match)
        if(req.body.match || req.body.must.match === undefined )
            return res.status(500)
        let options = {
			should: [],
			must: [],
			must_not: [],
			limit: req.query.limit || 10,
			offset: req.query.offset || 0,
			order: [{ name: { order: 'desc' } }],
        };
        
        // if(req.body.should)
        //     options.should = options.should.concat(req.body.should);
        if(req.body.must)
            options.must = options.must.concat(req.body.must)
        req.options = options;
        next();
    }
}