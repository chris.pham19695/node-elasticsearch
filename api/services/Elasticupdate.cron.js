const cron = require('node-cron');
const AutoUpdateElastic = require('./../middlewares/elasticUpdate/autoUpdateElastic')


cron.schedule("0 4 * * *", async () => {
	console.log("each day at four - UPDATING ELASTIC SEARCH");
	try {
		await AutoUpdateElastic.updateObjectives();
		await AutoUpdateElastic.updateUser();
		console.log("update everyminutes");
	} catch (e) {
		console.log(e);
		throw "elastic cron  failed";
	}
});
