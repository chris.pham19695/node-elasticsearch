const express    = require('express');
const bodyParser = require('body-parser');
const apiRouter  = require('./routes')
const cors       = require('cors');
//const cron       = require('./services/Elasticupdate.cron');



var app = express();
//rabbit
//cors
app.use(cors());
//body parser
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
app.use(bodyParser.json({ limit: '10mb' }));
//-----------------------------------
//App api
app.use('/api', apiRouter);
//-----------------------------------
app.use(function(req, res) {
    return res.status(404).json({ error: 'Page not found' });
});

module.exports = app;
