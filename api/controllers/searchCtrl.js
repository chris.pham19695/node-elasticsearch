const ElasticService = require('./../services/elastic.util');
const successMess = require('./../services/errors/success');

module.exports = {
    objectives: async (req, res, next) => {
        let returns = {};
        let results = await ElasticService.search('objectives', req.options.must, req.options.must_not, req.options.should, req.options.limit,
        req.options.offset, req.options.order );    
        req.status = 200;
        returns.data = results.hits.hits;
        returns.total = results.hits.total;
        returns.global = successMess[200].objectives_found
        req.return = returns
        next();
    },
    users: async (req, res, next) => {
        let returns = {};
        let results = await ElasticService.search('users', req.options.must, req.options.must_not, req.options.should, req.options.limit,
        req.options.offset, req.options.order );  
        req.status = 200;
        returns.data = results.hits.hits;
        returns.total = results.hits.total;
        returns.global = successMess[200].users_found
        req.return = returns
        next();
    },
}